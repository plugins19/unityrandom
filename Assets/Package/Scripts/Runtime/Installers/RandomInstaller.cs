using AttaboysGames.Package.Scripts.Runtime.Providers.Impls;
using Zenject;
using UnityEngine;

namespace AttaboysGames.Package.Scripts.Runtime.Installers
{
	[CreateAssetMenu(menuName = "Installers/Attaboys/" + nameof(RandomInstaller), fileName = nameof(RandomInstaller))]
	public class RandomInstaller : ScriptableObjectInstaller
	{
		public override void InstallBindings()
		{
			Container.BindInterfacesTo<UnityRandomProvider>().AsSingle();
		}
	}
}
