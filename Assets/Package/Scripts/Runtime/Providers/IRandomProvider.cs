using UnityEngine;

namespace AttaboysGames.Package.Scripts.Runtime.Providers
{
	public interface IRandomProvider
	{
		Vector2 InsideUnitCircle { get; }

		Vector3 InsideUnitSphere { get; }

		Vector3 OnUnitSphere { get; }

		Quaternion Rotation { get; }

		Quaternion RotationUniform { get; }

		Random.State State { get; }

		float Value { get; }

		Color ColorHSV();

		Color ColorHSV(float hueMin, float hueMax);

		Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax);

		Color ColorHSV(
			float hueMin,
			float hueMax,
			float saturationMin,
			float saturationMax,
			float valueMin,
			float valueMax
		);

		Color ColorHSV(
			float hueMin,
			float hueMax,
			float saturationMin,
			float saturationMax,
			float valueMin,
			float valueMax,
			float alphaMin,
			float alphaMax
		);

		void InitState(int seed);

		float Range(float minInclusive, float maxInclusive);

		float Range(float inclusive);

		int Range(int minInclusive, int maxInclusive);

		int Range(int inclusive);
	}
}