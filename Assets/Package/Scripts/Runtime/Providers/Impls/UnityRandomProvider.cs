using UnityEngine;

namespace AttaboysGames.Package.Scripts.Runtime.Providers.Impls
{
	public class UnityRandomProvider : IRandomProvider
	{
		public Vector2 InsideUnitCircle => Random.insideUnitCircle;

		public Vector3 InsideUnitSphere => Random.insideUnitSphere;

		public Vector3 OnUnitSphere => Random.onUnitSphere;

		public Quaternion Rotation => Random.rotation;

		public Quaternion RotationUniform => Random.rotationUniform;

		public Random.State State => Random.state;

		public float Value => Random.value;

		public Color ColorHSV() => Random.ColorHSV();

		public Color ColorHSV(float hueMin, float hueMax) => Random.ColorHSV(hueMin, hueMax);

		public Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax)
			=> Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMin);

		public Color ColorHSV(
			float hueMin,
			float hueMax,
			float saturationMin,
			float saturationMax,
			float valueMin,
			float valueMax
		) => Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMin, valueMin, valueMax);

		public Color ColorHSV(
			float hueMin,
			float hueMax,
			float saturationMin,
			float saturationMax,
			float valueMin,
			float valueMax,
			float alphaMin,
			float alphaMax
		) => Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMin, valueMin, valueMax, alphaMin, alphaMax);

		public void InitState(int seed) => Random.InitState(seed);

		public float Range(float minInclusive, float maxInclusive) => Random.Range(minInclusive, maxInclusive);
		public float Range(float inclusive) => Random.Range(0, inclusive);

		public int Range(int minInclusive, int maxInclusive) => Random.Range(minInclusive, maxInclusive);

		public int Range(int inclusive) => Random.Range(0, inclusive);
	}
}